#!/bin/bash

docker pull simonsapathy/kubernetes-workshop:latest
docker run -d --rm --name kubernetes-intro -p 8000:8000 simonsapathy/kubernetes-workshop:latest

sleep 10

xdg-open http://localhost:8000 &>/dev/null
