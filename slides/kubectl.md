### Controlling Kubernetes


##### `kubectl`
* The client tool for interacting with Kubernetes REST API
* Tons of functionality
* Pronounced:
  + _cube C T L_
  + _cube C D L_
  + _cube cuddle_
  + _kube C T L_



#### Kubernetes Control Plane
* kubectl
   - client API interface for kubernetes control plane
* api server ![control-plane](img/k8s-master-control.png "Kubernetes Control Plane") <!-- .element: class="img-right"  width="60%"-->
   - REST frontend
+ controller manager
   - replication
   - deployment
+ etcd
   - key/value storage

<!-- .element: style="font-size:19pt;"  -->



#### Inline documentation
<code>kubectl </code><code style="color:red;">-h</code>
* Use `-h` option to get an overview of commands 
   <pre style="font-size:10;"><code data-trim data-noescape>
   $ kubectl -h  
   kubectl controls the Kubernetes cluster manager. 
   
   Find more information at: https://kubernetes.io/docs/reference/kubectl/overview/
   
   Basic Commands (Beginner):
     create         Create a resource from a file or from stdin.
     expose         Take a replication controller, service, deployment or pod and
</code></pre>   
<!-- .element: class="stretch"  -->


#### Command documentation
<code>kubectl </code><code style="color:green;">COMMAND </code><code>-h</code>
* Get usage for any command
<!-- .element: class="stretch"  -->

```
$ kubectl run -h
Create and run a particular image, possibly replicated. 

Creates a deployment or job to manage the created container(s).

Examples:
  # Start a single instance of nginx.
  kubectl run nginx --image=nginx
```
<!-- .element: class="fragment" data-fragment-index="0" style="font-size:12pt;" -->


#### Get documentation about resources in Kubernetes
<code>kubectl explain </code><code style="color:red;">RESOURCE</code>
* In Kubernetes we manage objects, or _resource types_ eg.
   + nodes
   + pods
   + namespaces
* The <!-- .element: class="fragment" data-fragment-index="0" -->`explain` command displays documentation about specific kubernetes resources 



#### Exercise: Ask `kubectl` about resources
* Use `kubectl explain` to find out about
   + nodes
   + namespaces
   + pods
   + services
* These are typical _resource types_ in Kubernetes

```
$ kubectl explain node
KIND:     Node
VERSION:  v1

DESCRIPTION:
     Node is a worker node in Kubernetes. Each node will have a unique
     identifier in the cache (i.e. in etcd).

FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
```
<!-- .element: class="fragment" data-fragment-index="0" style="font-size:10pt;" -->



#### Configuring `kubectl`

* As mentioned before, the openstack CLI can configure this for us

```
$ openstack coe cluster list
+--------------------------------------+-----------+--------------+------------+--------------+-----------------+---------------+
| uuid                                 | name      | keypair      | node_count | master_count | status          | health_status |
+--------------------------------------+-----------+--------------+------------+--------------+-----------------+---------------+
| 81969b37-e3b7-4a6c-9112-03ed194e456a | trainpc03 | training-key |          2 |            1 | CREATE_COMPLETE | UNKNOWN       |
+--------------------------------------+-----------+--------------+------------+--------------+-----------------+---------------+
$ eval $(openstack coe cluster config 5c2d36b4-9f9c-43cf-8411-c5a966b5d888)
$ kubectl cluster-info
Kubernetes master is running at https://150.242.43.144:6443
CoreDNS is running at https://150.242.43.144:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```
<!-- .element: style="font-size:10pt;" -->
* Try that `eval` command now if the the status `CREATE_COMPLETE`


#### That config file..

* It's a mostly certificates and metadata:

```
$ cat config 
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUMyVENDQWNHZ0F3SUJBZ0lSQUxEUllkWmVma3Fpb3paNllDUXQ3UUl3RFFZSktvWklodmNOQVFFTEJRQXcKRkRFU01CQUdBMVVFQXd3SmRISmhhVzV3WXpBek1CNFhEVEl3TURZd09ERTRORFl6TmxvWERUSTFNRFl3T0RFNApORFl6Tmxvd0ZERVNNQkFHQTFVRUF3d0pkSEpoYVc1d1l6QXpNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DCkFROEFNSUlCQ2dLQ0FRRUF3THRCYmsrTVlEaXBLcXpyaGtldnFNalp2OFV2a2hIV3I0Z2xrR0tqeUdVdFF0dkQKYXJoMS9QWnQveVNtb0VmTVpPK21aaEpWakJvaGFKTlNXRnJXaElLaHRuWHNCbG5tYWJvdDV4aW13VzNhZmpsRQptRGxSc3o5TlBobURhbmNpTFgrTVdSZXBWRFQ4WTJKSzI5OFNoQlFWWGxsV0txby8xUW5uUmdITUNNRG53MnJMCmxVdWtZSURwVVI1NC85RWdLSXNCM3VNN29MUkpUQjRaU2VneUhEUXlJaUV5TkZKenRXZXRQbW4wNXBrd0ZyS2EKcmt5Z0M2VEZYbDB5cHJVZEtrV0RLM3hGRWtZWXZOY0o5cUp4eElqaFk5dnh2eEVSQ2xQY2w4d2FMZW1lbGpLTAphaHVoQ2Q0UGRjSmIyOFVoTThmYThjeXVmRzkraHIvbk1UU0FJd0lEQVFBQm95WXdKREFTQmdOVkhSTUJBZjhFCkNEQUdBUUgvQWdFQU1BNEdBMVVkRHdFQi93UUVBd0lDQkRBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQXNlMEcKVkFwbVc4d2t3SElzWU1YSkJQM0pBUWVDMkhPWFZ5VmVWTi9UQ0g3Y2JwSmJiMkhpZTYxRS93bS9iZHJrYi80dQpZK3ppUkh6akdRWU5Kd0N0L2lOcG94S2tNSm16OFMxQTZVK2M2RGZaQUhTUk1aM1c1ZElTM0h0QlR3clJTY0EyCkVsbmsza1BaTTgycnpibjVoZUUrODNLQXV1eTRJSXB6K2FnR2Q1YW54TUFITzVuVWFmWFJwM0F5aGhZem1qaGoKRFUxYVMxaHJDMFVicTRLYkZPMnNyVEhrTHA3UXFueFFyVTEvYUpZM1QxbUZ6a2NpZi9HelhDVXFZc3dQalltcgpFc1lIdGpxSExZMFcwbi81NVNMU0ZNbmswcFlhWVRkZXJNaEVMenZqTmtNZlVoME9DQVdNWkhaY3AwV0RUWnI1Cjd6L2IyNXNMT3RYQWdVQlZSZz09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0=
    server: https://150.242.43.144:6443
  name: trainpc03
contexts:
- context:
    cluster: trainpc03
    user: admin
  name: default
current-context: default
kind: Config
preferences: {}
users:
- name: admin
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN4VENDQWEyZ0F3SUJBZ0lRSDNneEZiaVNUb2U1NE02MnRhb3ZlekFOQmdrcWhraUc5dzBCQVFzRkFEQVUKTVJJd0VBWURWUVFEREFsMGNtRnBibkJqTURNd0hoY05NakF3TmpBNE1UZzFPVEkwV2hjTk1qVXdOakE0TVRnMQpPVEkwV2pBcE1RNHdEQVlEVlFRRERBVmhaRzFwYmpFWE1CVUdBMVVFQ2d3T2MzbHpkR1Z0T20xaGMzUmxjbk13CmdnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0SUJEd0F3Z2dFS0FvSUJBUUNwNmJmK1E4RzNxcDFjd0J5WWIxY3IKcFFvbmhySXJyeXVxZzJVZGJuM1I4eUdtdC9aTHJHVWlSWlBIU2pEYmxnNVVqNEJ0ZG15eXo0R2MwM3pNbEkyMQpwOGVVZUVBc3k2RmZLUEk0YitqVS9aVFVJWnphSzhINEk5OWtFSlE1MVZPekc4U0ptOXdVSUNEQVRuSE9IMlNLCklnRHJLaTVKaysvK3N0Y3liTlVQTmFhVFdUL0pjNk9HV24zUzlWTU13NDVHL3B3NkR2WXlBV0tGL052T3o1K3gKUGQ0MDNKWUdORWZQMW5oZ0ZIMEZvSk80VEJBakVySWZKQVMyN0xUR1kvZE13TittSis0Z1VKVVNHRy9aU2wvaQphazJXTTA5L1EzeUFLR3NOTExrbnQ2S25sYjBHYVdUeXNrdVN0bk82OE9PQjE4SWlXamQ3cExTZHBHdEtSNDRuCkFnTUJBQUV3RFFZSktvWklodmNOQVFFTEJRQURnZ0VCQUwwSkpjeWJzenB5ZDl1MHJXckJJbElNNHRHelJoOWQKNjNoNXFYMmJqcXNET24vbWYxZnlrOXcwR1loUUo4c0Y3MnV0UjIyTGNtNnIyem5XVC9nSmUyK2wrUFRjYjBkOApwRStiUEkrRC9wYzZxQXV5NERUVWNvL1hXQ1ZYTERadlc1WkpHNWpWYUZua1llUXNWRUJCcno3WVE0aXpXQWVUClpnU0c0OC9hNm15MVRyd0w1RWZsZWUzcWhyQjBveU9oNTJiSHhCNDVKVjF1QzlKbTh2bGdjZjE1MEJUdkhpZloKT3kvMStnbmUzUGd0amZkODI4VlU4eVBtWmhkb2R3UGUwenR0STZ3Vko4ZHQ3MDdGYWxwRG81TWIrSDNBaGl5MwprbitMYnRrMHJRck5rOVdhUHRIT1FPVmFTa2VDS2dqL3NMRmt0bUcwRVorM3pNQXo0MXhjVEdFPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t
    client-key-data: LS0tLS1CRUdJTiBSU0 (etc..)
```
<!-- .element: style="font-size:10pt;" -->
