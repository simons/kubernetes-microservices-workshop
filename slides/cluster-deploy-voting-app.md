### Wholesale microservices application deployment
* ... And a brief diversion into ConfigMaps



#### Setting up the Voting Application
* Have a look at kubernetes specs for the vote app
   ```
   cd ~/kubernetes-introduction/sample-code/vote-app-exercise
   ls ...
   ```
   <!-- .element: style="font-size:13pt;" -->
* Folder contains specification files for 
   + Deployments
   + Services


#### Creating a namespace
   ```
   $ kubectl create namespace vote
   ```

   ```
   $ kubectl get namespace
   ```


#### Deploy entire app

* You can point kubectl at a whole directory
   
   ```
   $ kubectl apply -f .
   configmap/postgres-config created
   deployment.apps/db created
   service/db created
   deployment.apps/redis created
   service/redis created
   deployment.apps/result created
   deployment.apps/vote created
   service/vote created
   deployment.apps/worker created
   ```

* <!-- .element: class="fragment" data-fragment-index="0" -->
   Watch what is happening
   
   ```
   watch kubectl -n vote get all
   ```
   <!-- .element: class="fragment" data-fragment-index="0" -->


#### Viewing Vote Website
* <!-- .element: class="fragment" data-fragment-index="0" -->Once all containers are running you can visit the *vote* app
* <!-- .element: class="fragment" data-fragment-index="1" -->We exposed the vote part with a LoadBalancer...

```
$ kubectl get services --namespace vote
NAME    TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
db      ClusterIP      10.254.240.67   <none>          5432/TCP       17m
redis   ClusterIP      10.254.18.144   <none>          6379/TCP       17m
vote    LoadBalancer   10.254.229.24   202.49.241.51   80:30729/TCP   17m
```
<!-- .element: class="fragment" data-fragment-index="1" -->


#### Exercise: Scale number of replicas for vote

* Increase the number of replicas (pods) for the _vote_ service
* <!-- .element: class="fragment" data-fragment-index="0" -->In the terminal
   <pre class="fragment" data-fragment-index="1"><code data-trim data-noescape>
      kubectl -n vote <mark>scale deployment vote</mark> --replicas=3
    </code></pre>
* Keep an eye on <!-- .element: class="fragment" data-fragment-index="3" -->_watcher_ terminal 
* Try varying number of replicas up and down<!-- .element: class="fragment" data-fragment-index="4" -->
* Don't scale <!-- .element: class="fragment" data-fragment-index="5" -->_worker_ yet..
* <!-- .element: class="fragment" data-fragment-index="6" -->Scale it back down to 1 before moving on


##### Exercise:  Update voting app
* Update the _vote_ application with your image
   <pre><code data-trim data-noescape>
   kubectl -n vote <mark>set image</mark> deployment/vote \
            vote=YOURNAME/vote:v2
  </code></pre>
* Watch the _watcher_ terminal
* Refresh the site several times while update is running
* Try it by editing vote-deployment.yaml too<!-- .element: class="fragment" data-fragment-index="1" -->


##### Exercise:  Create a LoadBalancer Service for results too
* Let's see how you get on..


##### Exercise:  Are you votes being recorded?
* Probably not! How do we diagnose this?
* Use some combination of
   + kubectl logs
   + kubectl get pods
   + The problem is in the vote namespace


##### Let's fix postgres!
* So postgres isn't letting the worker connect, let's change Postgres' configuration badly
* Edit db-configmap.yaml
* Change POSTGRES_HOST_AUTH_METHOD to trust
* And delete the db and worker pods to restart them
```
kubectl delete -n vote pod worker-5764d777cd-spnnn db-f66655d67-554jv
```


#### Summary
* In this section we deployed a microservice application
* kubectl can scale services up or down
* Deployment rolling updates ensure that the application updates seemlessly
  with zero downtime
* Used our log reading skills to identify a problem
* Updated a ConfigMap
