### Kubernetes Resources


#### Kubernetes Resource Types
* Nodes
* Pod
* Deployment
* Namespaces
* Services
* ... And a lot lot more

```
$ kubectl api-resources
```
<!-- .element: class="fragment" data-fragment-index="0" style="font-size:10pt;" -->
* Note that 'NAMESPACED' coloumn, we'll talk about that later
<!-- .element: class="fragment" data-fragment-index="0" style="font-size:10pt;" -->


#### Nodes
* Nodes are where your containerised workloads will run![Orchestration](img/container-orchestration.svg "Container Orchestration")<!-- .element: class="img-right" width="50%" -->
* No upper limit on number of nodes in a Kubernetes cluster <!-- .element: class="fragment" data-fragment-index="0" -->
* Let's have a look at our Kubernetes Cluster on the Catalyst Cloud <!-- .element: class="fragment" data-fragment-index="1" -->



#### Displaying Resources
<code>kubectl get </code><code style="color:red;">RESOURCE</code>
* Retrieve information about kubernetes resources <!-- .element: class="fragment" data-fragment-index="0" -->
    + eg. nodes


##### Exercise: Using `kubectl get`
* Use `kubectl get` to get info about current nodes
   ```
   $ kubectl get nodes
   ```
   <!-- .element: class="fragment" data-fragment-index="0" style="font-size:10pt;"-->
   ```
   NAME                              STATUS   ROLES    AGE   VERSION
   trainpc03-3tbgc7fjgghl-master-0   Ready    master   56m   v1.16.9
   trainpc03-3tbgc7fjgghl-node-0     Ready    <none>   54m   v1.16.9
   trainpc03-3tbgc7fjgghl-node-1     Ready    <none>   54m   v1.16.9

   ```
   <!-- .element: class="fragment" data-fragment-index="1" style="font-size:10pt;" -->
* We've got 3 nodes, one with the role Master <!-- .element: class="fragment" data-fragment-index="1" -->
* The default stdout is minimal <!-- .element: class="fragment" data-fragment-index="2" -->


#### Formatting output
* Many `kubectl` commands can output different data formats
  + yaml
  + json
  + wide (Extra stuff, human readable)
* Pass `-o FORMAT` to command

```
Usage:
  kubectl get [(-o|--output=)json|yaml|wide|custom-columns=...|
   custom-columns-file=...|go-template=...|
   go-template-file=...|jsonpath=...|jsonpath-file=...]
```
<!-- .element: class="fragment" data-fragment-index="0" -->


##### Exercise: Get formatted data about nodes
* Output node information in JSON
   ```
   kubectl get nodes -o json
   ```
   ```json
   {
    "apiVersion": "v1",
    "items": [
        {
            "apiVersion": "v1",
            "kind": "Node",
            "metadata": {
                "annotations": {
                    "node.alpha.
                    "volumes.kub
   ```
   <!-- .element: class="fragment" data-fragment-index="0" -->
* Quite a bit more information to process <!-- .element: class="fragment" data-fragment-index="1" -->


##### Exercise: Process `kubectl` output
* It can be useful to pipe formatted output through other tools <!-- .element: class="fragment" data-fragment-index="0" -->
   + For example <!-- .element: class="fragment" data-fragment-index="1" -->[jq](https://stedolan.github.io/jq)
* Get a JSON list of node names with corresponding IP <!-- .element: class="fragment" data-fragment-index="2" -->
```
kubectl get nodes -o json | jq '.items[] |
{name: .metadata.name, ip: (.status.addresses[]  |
select(.type == "InternalIP")) | .address }'
```
<!-- .element: class="fragment" data-fragment-index="3" style="font-size:10pt;" -->
```json
{
  "name": "minikube",
  "ip": "10.0.2.15"
}
```
<!-- .element: class="fragment" data-fragment-index="4" -->


##### For human readibility, you might prefer YAML
* Output node information in YAML
   ```
   kubectl get node trainpc03-3tbgc7fjgghl-master-0 -o YAML
   ```
   ```
   apiVersion: v1
   kind: Node
   metadata:
   annotations:
      node.alpha.kubernetes.io/ttl: "0"
      projectcalico.org/IPv4Address: 10.0.0.10/24
      volumes.kubernetes.io/controller-managed-attach-detach: "true"
   creationTimestamp: "2020-06-09T06:51:47Z"
   labels:
      beta.kubernetes.io/arch: amd64
      beta.kubernetes.io/instance-type: c093745c-a6c7-4792-9f3d-085e7782eca6
   ```
   <!-- .element: style="font-size:10pt;" -->
