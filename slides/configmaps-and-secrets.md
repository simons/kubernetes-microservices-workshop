### ConfigMaps and Secrets

#### Injecting configuration into our Services


#### ConfigMaps

* ConfigMaps exist so you can configure your application
    + You can express them as files
    + Environment variables (See 12 Factor App)
    + Or collect them straight from the API


#### As Environment Variables

* Our broken Postgres was configured like this
```
db-deployment.yaml
```
<!-- .element: style="font-size:10pt;" -->
```
      - image: postgres:9.4
        name: postgres
        envFrom:
        - configMapRef:
            name: postgres-config
```
<!-- .element: style="font-size:10pt;" -->
```
db-configmap.yaml
```
<!-- .element: style="font-size:10pt;" -->
```
apiVersion: v1
kind: ConfigMap
metadata:
    name: postgres-config
    namespace: vote
data:
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: postgres
    POSTGRES_HOST_AUTH_METHOD: deny
```
<!-- .element: style="font-size:10pt;" -->


#### As files

* Let's create a configmap with 2 files in it
```
cd ~/kubernetes-introduction/sample-code/ConfigMaps
```
<!-- .element: style="font-size:10pt;" -->
* Look at the files in here
* Create a configmap from the text files
```
kubectl create configmap config-files --from-file file1.txt --from-file file2.txt
```
<!-- .element: style="font-size:10pt;" -->
* Create a Pod with these ConfigMaps mounted
```
kubectl apply -f example-configmap-files.yaml
```
<!-- .element: style="font-size:10pt;" -->


#### Look inside..

```
kubectl exec -ti config-test-pod -- sh
```
<!-- .element: style="font-size:10pt;" -->
* Have a look in /etc/config
* Exit out, try editing the ConfigMap
* Did it change inside the Pod?


#### Secrets are pretty much the same

* Because they are a different resource type, you can allow or dissallow roles access
    + Can be per namespace
    + Can be clusterwide
    + So you could allow read access to all ConfigMaps, but no secrets
* Plus there is a special 'TLS' type


#### ConfigMaps and Secrets allow you to seperate config and code

* Allows you to deploy the same container image everywhere
    + Staging
    + Dev
    + Prod
    + DR Systems
    + The moon!
* ... And only update the configuration, so you can have higher confidence in your deployment
