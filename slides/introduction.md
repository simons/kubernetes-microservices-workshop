# Catalyst <!-- .element: class="catalyst-logo" -->
### Deploying Microservices with Kubernetes
Course Originally By <!-- .element: class="small-text" --> [Travis Holton](Link) <!-- .element: class="small-text" -->

Modified and Presented by <!-- .element: class="small-text" --> Simon Story <!-- .element: class="small-text" -->


## Deploying Microservices with Kubernetes <!-- .slide: class="title-slide" --> <!-- .element: class="orange" -->
