### Running Workloads in Kubernetes


#### Running Containerised Workloads
<code style="font-size:16pt;">kubectl create </code><code style="color:blue;font-size:16pt;"> deployment </code><code style="color:red;font-size:16pt;">name </code><code style="color:red;font-size:16pt;">--image=IMAGE:TAG</code><code style="color:green;font-size:16pt;"> OPTIONS</code>
* Create and run jobs in Kubernetes
* Example<!-- .element: class="fragment" data-fragment-index="0" -->:
   ```
   kubectl create deployment nginx --image=nginx
   ```
* <!-- .element: class="fragment" data-fragment-index="1" -->Use **`get`** command to find out about container we just started
   ```
   kubectl get containers
   ```


#### UH OH...
* <!-- .element: class="fragment" data-fragment-index="3" -->Container isn't actually a resource type in Kubernetes
   ```
   error: the server doesn't have a resource type "container"
   ```


#### Open the Pod bay doors HAL
* Technically you do not run  <!-- .element: class="fragment" data-fragment-index="0" -->_containers_ in Kubernetes
* The atomic <!-- .element: class="fragment" data-fragment-index="1" -->_run unit_ of Kubernetes is called a *_Pod_* 
* A Pod is an abstraction representing group <!-- .element: class="fragment" data-fragment-index="2" -->of ≥ 1 containers
   - images![pod and services](img/k8s-pods.png "Pods") <!-- .element: class="img-right" style="width:50%;" -->
   - network ports
   - volumes
* In this lesson we'll be using single container pods <!-- .element: class="fragment" data-fragment-index="3" -->


#### Pods
* Containers in a Pod share common resources   
   - Network IP address ![pod-anatomy](img/k8s-pod-anatomy.png "Pod upclose") <!-- .element: class="img-right" -->
   - Can share mounted volumes
   - The nature of the system means they are co-located and co-scheduled
* Containers within a Pod communicate via _localhost_
* A example use for multi-container pod is a webserver and a language interpreter
   + Eg For PHP or Python maybe you'd run them in the same pod as a Nginx or Apache


##### Exercise: Gather info about pods
* Use `kubectl get` to find info about running pods
   ```
   kubectl get pods
   ```
   ```
   NAME                     READY     STATUS    RESTARTS   AGE
   nginx-65899c769f-ttt2x   1/1       Running   0          1h
   ```
   <!-- .element: class="fragment" data-fragment-index="0" style="font-size:13pt;" -->


#### Running a Pod
* Let's run a <!-- .element: class="fragment" data-fragment-index="1" -->_ping_ command against Cloudflare's public DNS resolver
   ```
   kubectl run pingy --image=busybox -- ping 8.8.8.8
   ```
   ```
   pod/pingy created
   ```
* So, what is happening? <!-- .element: class="fragment" data-fragment-index="3" -->


#### View logs for a pod
* The **`logs`** command behaves the same as with `docker logs`
* Query logs for pingy
   ```
   kubectl logs pingy
   ```

|Option  | Description |
|--- | --- |
| -f, --follow | stream logs similar to `tail -f` |
| --tail <integer> | Specify how many lines from end to start with |
| --since | Get logs after a timestamp |


#### Watching pods
* The **`-w`** option to kubectl is like the Linux **`watch`** command
   ```
   kubectl get pods -w
   ```
* In another window run the following:
   ```
   kubectl delete pod/pingy
   ```


#### Scheduling Pods

* The <!-- .element: class="fragment" data-fragment-index="0" -->`--schedule=` option takes a cron-like time pattern
* Creates a type of pod that runs periodically at assigned times <!-- .element: class="fragment" data-fragment-index="1" -->
* In other words, a cronjob <!-- .element: class="fragment" data-fragment-index="2" -->

```
kubectl create cronjob my-job --image=busybox --schedule="*/1 * * * *" -- date
```
<!-- .element: class="fragment" data-fragment-index="3" -->

* This pod outputs the time every minute <!-- .element: class="fragment" data-fragment-index="4" -->
* Use kubectl logs to see this <!-- .element: class="fragment" data-fragment-index="5" -->
