### Ingress Controllers
... And brief diversion into Helm


#### Ingress Controller
* A Kubernetes resource type
* Alternative way to expose workloads in a cluster
* Runs in your cluster and routes traffic to container workloads ![basic-ingress-controller](img/ingress-controller.png "Basic Ingress Controller")<!-- .element: class="img-right" -->



#### Setting up Ingress
* <!-- .element: class="fragment" data-fragment-index="0" -->Add the nginx repository
   ```
   helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
   helm repo update
   ```
* <!-- .element: class="fragment" data-fragment-index="1" -->Use helm to install an ingress controller
   ```
   helm install ingress-nginx ingress-nginx/ingress-nginx
   ```
   - should generate a bunch of output
* <!-- .element: class="fragment" data-fragment-index="2" -->Watch cluster to
  see when new LB is created
   ```
   kubectl get services -o wide ingress-nginx-controller -w
   ```
* <!-- .element: class="fragment" data-fragment-index="3" -->Note the **EXTERNAL-IP** that is created



#### Ingress and LoadBalancer
* <!-- .element: class="fragment" data-fragment-index="0" -->You still need a LoadBalancer to route traffic to nodes ![ingress-lb](img/ingress-controller-with-lb.png "opt title") <!-- .element: class="img-right" width="50%" -->
* <!-- .element: class="fragment" data-fragment-index="1" -->The advantage of Ingress is how it can route traffic to your appliations



#### Ingress routing
##### Path-based routing
![path-routing](img/ingress-controller-route-path.png "Ingress path routing")


#### Exercise: Create our first ingress
* <!-- .element: class="fragment" data-fragment-index="0" -->Let's run the cat app in our cluster
   ```
    kubectl delete ns cats # Deletes everything the cats namespace
    kubectl create ns cats
    kubectl -n cats create deployment cat-app\
    --image=simonsapathy/cotd:v1
    kubectl -n cats expose deployment cat-app\
    --port=5000
   ```
* <!-- .element: class="fragment" data-fragment-index="1" -->Let's also start an nginx pod and expose it on port 80
   ```
   kubectl -n cats create deployment nginx --image=nginx
   kubectl -n cats expose deployment nginx --port=80
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->Add **my-cats.com** to `/etc/hosts` with the external IP of your
  loadbalancer
  * The sudo password is 'train'
  ```
  xxx.xxx.xxx.xxx   my-cats.com
  ```


#### Expose our ingress using paths
* Put the following in a file called `cats-ingress.yml`
<pre style="font-size:8pt;"><code data-trim data-noescape>
  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
    name: cat-ingress
    namespace: cats
    annotations:
      kubernetes.io/ingress.class: nginx # or "cove.nginx" if you are on the Cove course
      nginx.ingress.kubernetes.io/rewrite-target: /
  spec:
    rules:
      - host: my-cats.com
        http:
          paths:
            - path: /cats
              pathType: Prefix
              backend:
                service:
                 name: cat-app
                 port:
                   number: 5000
            - path: /hello
              pathType: Prefix
              backend:
                service:
                 name: nginx
                 port:
                   number: 80
</code></pre>


#### Create the ingress
* Create the ingress
   ```
   kubectl create -f cats-ingress.yml
   ```


#### Ingress routing
##### domain-based routing
![subdomain-routing](img/ingress-controller-route-sub.png "Ingress subdomain routing")



#### Define an ingress for vote app
* This can also be done with a spec file
<pre style="font-size:12pt;"><code data-trim data-noescape>
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: vote-ingress
      namespace: vote
      annotations:
        kubernetes.io/ingress.class: nginx
    spec:
      rules:
        - host: vote.my-app.com
          http:
            paths:
              - backend:
                  serviceName: vote
                  servicePort: 80
</code></pre>


#### Setup domain-based ingress
* First, add a couple more hosts to /etc/hosts
  <pre style="font-size:12pt;"><code data-noescape>
  xxx.xxx.xxx.xxx   my-cats.com <mark>vote.my-app.com result.my-app.com</mark>
  </code></pre>

* Save out the previous slide's yaml to a file and apply if
* `kubectl apply -f ingress.yaml`


#### Exercise:
* Can you make make a domain based ingress for the result service too?


#### Let's talk about Helm, briefly
* Helps you install complex apps without having to modify Kubernetes manifests
* Uses templates to create Kubernetes manifests and add install them in a kubernetes cluster
* Can do the sorts of things you expect a template language to do
* Eg. Plug in values, do loops, if/else if logic
* Also can help with indentation, which is important for YAML


#### Let's look at the Nginx Helm Chart

* https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx
* Look at values.yaml - it's the values to be plugged into the template
* Look in templates
* Especially default-backend-service.yaml, controller-configmap.yaml and controller-deployment.yaml
* Last one is bit hard to read I know


#### Other stuff about Helm
* Uses labels to track what it has deployed: `helm list`
* Helm 3 does not run any pods in your cluster, relies on your own permissions
* NOTES.txt can be used to print helpful output
* Doesn't build Docker images


#### Summary
* Ingress controllers are a versatile and cost-effective alternative to LoadBalancer service
* A Load Balancer per thing would cost you a lot more
* Can route traffic for multiple sites
  - path-based routing
  - subdomain-based routing
  - HTTPS Offload
