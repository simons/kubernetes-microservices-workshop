### Services


#### Services, where it starts to come together
* Routing traffic to pods!
   + Earlier we started an nginx pod
   + At the moment there is no way to reach it
   ```
   kubectl get pods -o json | jq '.items[] |\
   {name: .metadata.name, podIP: .status.podIP }'
   ```
   <!-- .element: style="font-size: 9pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="0" -->Response
   ```
   {
     "name": "nginx-b848f798f-clrvd",
     "podIP": "172.17.0.5"
   }
   ```
* The <!-- .element: class="fragment" data-fragment-index="1" -->_podIP_ is not reachable
* Outside or inside the cluster, it's hard to find



#### Services
* <!-- .element: class="fragment" data-fragment-index="0" -->A **service** defines logical set of pods and policy to access them ![pods service](img/k8s-service-pods1.png "basic service") <!-- .element: class="img-right" -->
* ensure pods for a specific deployment receive network traffic <!-- .element: class="fragment" data-fragment-index="1" -->



#### The `expose` command
<code>kubectl expose -h</code>
* Creates a service
* Takes the name (label) of a resource to use as a selector
* Can assign a port on the host to route traffic to our pod



##### Exercise: Expose nginx workload
* Need to create a service which
  + point to the _nginx_ deployment
  + maps requests to port on nginx pod (port 80)
  + opens a port that is visible throughout the cluster

```
kubectl expose deployment nginx --type=ClusterIP --port=80
```
<!-- .element: class="fragment" data-fragment-index="0" font-size:13pt; -->
```
service/nginx exposed
```
<!-- .element: class="fragment" data-fragment-index="1" font-size:13pt; -->


#### Get list of available services
* Get list of services

```
$ kubectl get services
NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.254.0.1        none         443/TCP   72m
nginx        ClusterIP   10.254.212.172    none         80/TCP    7s
```
<!-- .element: class="fragment" data-fragment-index="1" font-size:10pt; -->


#### Let's browse to it!
* Let's start a web browser in our cluster
```
$ kubectl run -ti --image alpine alpine-shell -- sh
If you don't see a command prompt, try pressing enter.
/ # 
/ # apk update ; apk add links
/ # links
```
* http://allhotkeys.com/links_browser_hotkeys.html
* Go to http://nginx (Press g)
* Kubernetes creates DNS records for us inside the cluster so services can find each other
   + We didn't have to specify an IP address

```
$ kubectl delete pod alpine-shell
```



#### Networking Pods
* Each Pod in k8s has its own IP<!-- .element: class="fragment" data-fragment-index="0" --> ![raw pod](img/k8s-raw-pod-ip.png "Raw Pod Networking") <!-- .element: class="img-right" -->
   + even on same node
* Pod IPs never exposed outside cluster <!-- .element: class="fragment" data-fragment-index="1" -->
* Pod IPs change often <!-- .element: class="fragment" data-fragment-index="2" -->
   + updates/rollbacks
   + routine health maintenance
* Need a way to reliably map traffic to Pods <!-- .element: class="fragment" data-fragment-index="3" -->


#### Labels & selectors
* labels are key/values assigned to objects
   + pods
* labels can be used in a variety of ways: ![pod-label](img/k8s-pod-label.png "pod label") <!-- .element: class="img-right" -->
   + classify object
   + versioning
   + designate as production, staging, etc.



#### Labels & Selectors
* Earlier we created a pod with label:<!-- .element: class="fragment" data-fragment-index="0" --> <code style="color:green;">name=nginx</code>
   <pre><code data-trim data-noescape>
   kubectl create deployment <mark>nginx</mark> --image=nginx
    </code></pre>
* Then we created a service that maps requests to the selector <!-- .element: class="fragment" data-fragment-index="1" --><code style="color:green;">name=nginx</code>
  <pre ><code data-trim data-noescape>
  kubectl expose deployment <mark>nginx</mark> --type=ClusterIP
  </code></pre>


#### Matching Services and Pods
* Labels provide means for _services_ to route traffic to groups of pods
* Services route traffic to Pods with certain label using _Selectors_ 
```
$ kubectl describe deployment nginx |grep ^Labels
Labels:                 app=nginx
$ kubectl describe service nginx |grep ^Labels
Labels:            app=nginx
```


#### Diagram that didn't fit..
![service-label-selector](img/k8s-service-label-selectors.png "Labels and Selectors") <!-- .element: class="img-right" -->


#### Service types
* _ClusterIP_
   - Exposes workload on an internal IP in the cluster
* _NodePort_
   - Expose workload on each node (assumes each node has public IP (Kinda usesless..)
* _LoadBalancer_
   - Expose workload through single public IP (We'll talk about them more later)


#### ClusterIP Service
 Exposes the Service on an internal IP in the cluster 
 ![clusterip-service](img/k8s-cluster-ip-port-service.hml.png "ClusterIP")


#### NodePort Service
Expose port on each node in cluster
 ![nodeport-service](img/k8s-nodeport-service.png "NodePort")


#### LoadBalancer Service
Use load balancer to route traffic to service
 ![loadbalancer-service](img/k8s-loadbalancer-service.png "LoadBalancer")
 


#### Service Specification Files
* It is possible to expose a Pod with a Service using <!-- .element: class="fragment" data-fragment-index="0" -->`kubectl expose` command line
* For most applications, common to define <!-- .element: class="fragment" data-fragment-index="1" -->_service specification_ files 
* <!-- .element: class="fragment" data-fragment-index="2" -->A YAML or JSON file that defines a service


#### ClusterIP Spec File
 ![clusterip-service](img/k8s-cluster-ip-port-service.hml.png "ClusterIP")
<!-- .element: style="width:40%;float:right;"  -->

<pre style="width:40%;float:left;"><code data-trim data-noescape>
apiVersion: v1
kind: Service
metadata:
  name: redis
spec:
  <span class="fragment" data-fragment-index="1"><mark>type: ClusterIP</mark></span>
  <span class="fragment" data-fragment-index="2">ports:
  - port: 6379
    targetPort: 6379</span>
  <span class="fragment" data-fragment-index="3">selector:
    <mark>app: redis</mark></span></code></pre>


##### Exercise: View service spec
* <!-- .element: class="fragment" data-fragment-index="0" -->The kubectl
  `get` command can be used to get spec for a service or other objects
* <!-- .element: class="fragment" data-fragment-index="1" -->Use it to view specification for nginx service
   <pre class="fragment" data-fragment-index="2"><code data-trim data-noescape>
   kubectl get svc nginx -o yaml
</code></pre>
* <!-- .element: class="fragment" data-fragment-index="3" -->Output spec to a file
   <pre><code data-trim data-noescape>
   kubectl get svc nginx -o yaml > nginx-service.yml
</code></pre>



##### Exercise: Create service using spec file
* The spec file generated earlier can be used to create a service
* <!-- .element: class="fragment" data-fragment-index="0" -->Delete the
  *nginx* service
  ```
  kubectl delete svc nginx
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->Create service using file
   ```
   kubectl create -f nginx-service.yml
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->Check that the
  service is up and the site is funtional
  ```
  kubectl get svc
  ```


##### Exercise: Edit a spec inline
* It's also possible to edit a spec inline 
   ```
   $ kubectl edit service nginx
   ```
* <!-- .element: class="fragment" data-fragment-index="0" -->This will open up an editor (eg. vim) so you can edit the service
* <!-- .element: class="fragment" data-fragment-index="1" -->Try raising the value for the port
```
$ kubectl describe service nginx
```


#### Connecting an Application
* Functioning application depends on![kubernetes interaction](img/kubernetes-user-interaction.svg "Kubernetes Architecture") <!-- .element: class="img-right" style="width:50%;" -->
   + Deployment
   + Pods
   + Services
   + Labels, selectors
