### Persistent Volumes


#### Block/File Storage is kind of an anti-pattern

* That's my view anyway
* Object storage is preferable
* Linux behaves badly when filesystems misbehave


#### Variations

* Different providers have different capabilities
    + All do block storage
    + AWS and Azure will do shared filesystem (NFS/CIFS)
* You can run your own NFS/CIFS server
* Many storage backends supported
    + CIFS / NFS / GlusterfS
    + https://kubernetes.io/docs/concepts/storage/storage-classes/
* Some have different capabilities
* Each node only has so much space, this will let you work with larger files


#### Let's quickly set one up

* Take a look in `sample-code/Storage`
* Apply it
```
kubectl apply -f storage-example.yaml
```
* We need to wait a little while
```
kubectl describe pvc task-pv-claim
```
* Use kubectl exec and browsh to examine what you've created