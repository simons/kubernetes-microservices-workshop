### Kubernetes


#### About Kubernetes

* Greek word for _helmsman_ or _pilot_
* Also origin of words like _cybernetics_ and _government_
* Inspired by _Borg_, Google's internal scheduling tool
* Play on _Borg cube_
* [Source](https://news.ycombinator.com/item?id=9653797)


#### What does it do for me?

* It's an Orchestration Platform for Containers
   + Or better yet <!-- .element: class="fragment" data-fragment-index="1" -->
   + An Orchestration Platform for Microservices <!-- .element: class="fragment" data-fragment-index="2" -->
* Kubernetes is not really suitable for non-Microservices <!-- .element: class="fragment" data-fragment-index="3" -->


#### What's next

* We will learn how to interact with and control Kubernetes <!-- .element: class="fragment" data-fragment-index="1" -->
* Become aquainted with core Kubernetes concepts <!-- .element: class="fragment" data-fragment-index="2" -->
    - Nodes
    - Pods
    - Services
    - ConfigMaps
    - etc.
