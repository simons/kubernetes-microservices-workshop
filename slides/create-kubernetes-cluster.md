### Creating a Kubernetes Cluster



#### ... On an OpenStack cloud provider

```
$ cat /usr/local/bin/create-cluster
#!/bin/bash

openstack coe cluster create \
  --cluster-template kubernetes-v1.16.9-prod-20200602 \
  --docker-volume-size 15 --node-count 2  \
  --floating-ip-enable \
   --keypair training-key --os-cloud docker-training \
    some-name
```
<!-- .element: style="font-size:10pt;" -->
* Go ahead and run that yourself now

```
$ source ~/venv/bin/activate # Makes openstack tools availible
$ create-cluster 
Request to create cluster 5c2d36b4-9f9c-43cf-8411-c5a966b5d213 accepted
```
<!-- .element: style="font-size:10pt;" -->


#### ... On another provider

* Azure for example

```
$ az aks create --resource-group myResourceGroup \
--name myAKSCluster --node-count 2 --enable-addons monitoring \
--generate-ssh-keys
```
<!-- .element: style="font-size:10pt;" -->


#### Authenticating (Openstack)

* On different cloud providers the method is a bit different
    + but the result will be the same

```
$ openstack coe cluster list
+--------------------------------------+-----------+--------------+------------+--------------+-----------------+---------------+
| uuid                                 | name      | keypair      | node_count | master_count | status          | health_status |
+--------------------------------------+-----------+--------------+------------+--------------+-----------------+---------------+
| 81969b37-e3b7-4a6c-9112-03ed194e456a | trainpc03 | training-key |          2 |            1 | CREATE_COMPLETE | UNKNOWN       |
+--------------------------------------+-----------+--------------+------------+--------------+-----------------+---------------+
$ eval $(openstack coe cluster config 5c2d36b4-9f9c-43cf-8411-c5a966b5d888)
```
<!-- .element: style="font-size:10pt;" -->


#### Authenticating (Azure)

```
az aks get-credentials --resource-group myResourceGroup --name myAKSCluster
```
<!-- .element: style="font-size:10pt;" -->

* Kubernetes kubectl confinguration file created for you


#### Authenticating (AWS EKS)

```
aws eks update-kubeconfig --profile my-aws-cli-profile --region the-region-name --name my-clustername
```
<!-- .element: style="font-size:10pt;" -->

* Kubernetes kubectl confinguration file created for you


#### ... We'll come back to this later

* While we wait for our clusters to create, we can learn some more
